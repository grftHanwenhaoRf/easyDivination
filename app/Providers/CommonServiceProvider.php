<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CommonServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     * 注册服务
     *
     * @return void
     */
    public function register()
    {
        // 注册Common服务
        // 服务名CommonService
        // 定义的路径 \App\Services\CommonUtils
        $this->app->singleton('CommonService', function () {
            return new \App\Services\CommonUtils();
        });
    }
}
