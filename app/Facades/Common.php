<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Common extends Facade
{
    // 外观模式
    // 静态类对应的服务实例(已构建为单例模式)
    protected static function getFacadeAccessor()
    {
        return 'CommonService';
    }
}
