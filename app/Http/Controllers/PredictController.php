<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class PredictController extends Controller
{
    //
    /**
    * GetPool
    * 获取预测池
    *
    * @return list
    */
    public function getPool()
    {
        $jsonPool = Redis::get('predict:pool');
        $pool = !empty($jsonPool)? json_decode($jsonPool):[];

        return response()->json(
            $pool,
            200
        );
    }
}
