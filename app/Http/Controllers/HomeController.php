<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function view;

class HomeController extends Controller
{
    /**
    * IndexDivination
    * 占卜入口
    *
    * @return <+Return+>
    */
    public function indexDivination()
    {
        return redirect('/#/index/divination');
    }

    /**
    * IndexZodiac
    * 星座入口
    *
    * @return <+Return+>
    */
    public function indexZodiac()
    {
        return redirect('/#/index/zodiac');
    }
}
