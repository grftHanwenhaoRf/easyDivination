<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Arr;
use Overtrue\Socialite\User as SocialiteUser;

class GlobalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = [];
        $user = new SocialiteUser([
            'id' => Arr::get($user, 'openid'),
            'name' => Arr::get($user, 'nickname'),
            'nickname' => Arr::get($user, 'nickname'),
            'avatar' => Arr::get($user, 'headimgurl'),
            'email' => null,
            'original' => [],
            'provider' => 'WeChat',
        ]);

        session(['wechat.oauth_user.default' => $user]);

        return $next($request);
    }
}

