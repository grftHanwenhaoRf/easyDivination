<?php

namespace App\Console\Commands;

use App\Facades\Common;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class PredictPoolUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:pool:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '更新预测池';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // 该命令的策略
        // 更新预测池
        // redis中生成新的12宫6爻列表
        try {
            $pool = [
                'Aries' => Common::getSixYao(),
                'Taurus' => Common::getSixYao(),
                'Gemini' => Common::getSixYao(),
                'Cancer' => Common::getSixYao(),
                'Leo' => Common::getSixYao(),
                'Virgo' => Common::getSixYao(),
                'Libra' => Common::getSixYao(),
                'Scorpio' => Common::getSixYao(),
                'sagittarius' => Common::getSixYao(),
                'Capricorn' => Common::getSixYao(),
                'Aquarius' => Common::getSixYao(),
                'Pisces' => Common::getSixYao(),
            ];
            Redis::set('predict:pool', json_encode($pool));
        } catch (Exception $e) {
            // throw '缓存更新异常';
            Log::info("缓存更新时异常:". $e->getMessage());
        }
    }
}
