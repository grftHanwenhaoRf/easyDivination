<?php

namespace App\Services;

use Ramsey\Uuid\Uuid;

/**
 * 通用工具服务类
 */
class CommonUtils
{
    /**
    * GetSixYao
    * 获取六爻字符串
    *
    * @return string
    */
    public function getSixYao()
    {
        $sixYao = "";
        for ($i = 0; $i < 6; $i++) {
            $sixYao .= hexdec(substr(Uuid::uuid4(), 12, 1))%2;
        }
        return $sixYao;
    }

    //写一些通用方法
}
