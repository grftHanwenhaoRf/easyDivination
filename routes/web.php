<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 向微信服务器发起授权 成功后才可进入第三方平台
Route::group(['middleware' => ['web', 'wechat.oauth']], function () {

    // 以下为第三方平台接口(自己的服务)
    Route::any('/user', [Controllers\WeChatController::class, 'user']);

    Route::any('/open-platform/serve', function () {
        $user = session('wechat.oauth_user.default'); // 拿到授权用户资料

        dd($user);
    });

    Route::get('/index', [Controllers\HomeController::class, 'indexDivination']);

    Route::get('/index/zodiac', [Controllers\HomeController::class, 'indexZodiac']);

    Route::get('/predictPool', [Controllers\PredictController::class, 'getPool']);
});


// 验证公众号发来的请求
Route::any('/wechat', [Controllers\WeChatController::class, 'serve']);
